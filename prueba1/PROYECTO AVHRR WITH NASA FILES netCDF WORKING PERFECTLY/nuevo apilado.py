# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 20:05:06 2020

@author: David
"""

from netCDF4 import Dataset
import rasterio
from rasterio.transform import from_origin
import numpy as np
import glob 

def find_nearest_x(longitude, point_x):
    longitude = np.asarray(longitude)
    idx = (np.abs(longitude - point_x)).argmin()
    return idx
def find_nearest_y(latitude, point_y):
    latitude = np.asarray(latitude)
    dist = (np.abs(latitude - point_y))
    idy = np.where(dist ==dist.min())[0]
    return int(idy)
dir_path = 'C:/Users/51922/Desktop/nuevo/PROYECTO AVHRR WITH NASA FILES netCDF WORKING PERFECTLY/'
output = dir_path + 'output/'
files = sorted(glob.glob(dir_path+'*.nc'))
transform = from_origin(-90, 0, 0.0416, 0.0416)
dd= []
# Open netCDF-4 file.
for i in files:
    nc = Dataset(i)
    latitude = nc.variables['lat'][:]
    longitude = nc.variables['lon'][:]
    sst = nc.variables['sst']
    fillvalue = sst._FillValue
    data = sst[:]

    posx0 = find_nearest_x(longitude,-90)
    posx1 = find_nearest_x(longitude,-70)
    posy0 = find_nearest_y(latitude,0)
    posy1 = find_nearest_y(latitude,-20)

    # new_x0 = longitude[posx0]
    # new_x1 = longitude[posx1]
    # new_y0 = latitude[posy0]
    # new_y1 = latitude[posy1]

    y = latitude[posy0:posy1]
    x = longitude[posx0:posx1]

    data_final = data[posy0:posy1,posx0:posx1]
    print(data_final.shape)
   
    dd.append(data_final)   ### crea una lista de todas los datos

    # profile = {'driver': 'GTiff', 'height': data_final.shape[0], 'width': data_final.shape[1], 'count': 1, 'dtype': str(data_final.dtype), 'transform': transform}
    # with rasterio.open(output+str(i[28:-3])+'.tif', 'w', crs='EPSG:4326', **profile) as dst:
    #     dst.write(data_final,1)
    
# Create raster stack
data_total = np.stack(dd)
        
profile = {'driver': 'GTiff', 'count': data_total.shape[0], 'height': data_total.shape[1], 'width': data_total.shape[2], 'dtype': str(data_total.dtype), 'transform': transform}
with rasterio.open(output+'data_total.tif', 'w', crs='EPSG:4326', **profile) as dst:
    dst.write(data_total)
     

ds = rasterio.open(output+'data_total.tif')

data = ds.read()
data[data == fillvalue] = 0

profile = {'driver': 'GTiff', 'count': ds.count, 'height': ds.height, 'width': ds.width, 'dtype': str(data_total.dtype), 'transform': transform}
with rasterio.open(output+'data_total_new.tif', 'w', crs='EPSG:4326', **profile) as dst:
    dst.write(data)
